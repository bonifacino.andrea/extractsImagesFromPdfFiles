# General Informations
###### Powered by Andrea Bonifacino ( [@a.bonifacino](https://gitlab.com/bonifacino.andrea) )
This script extracts all images from pdf files. It works from a root directory to its leaves. The images extracted will be saved in folders with the root folder's hierarchy.

## Dependencies
extractsImagesFromPdfFiles uses the bash command  **pdfimages** to work properly.

## Basic usage

```bash
$ bash extractsImagesFromPdfFiles.sh <rootfolder> <destinationfolder>
```
in which **\<rootfolder\>** is the root folder of the pdf files and **\<destinationfolder\>** is the destination root folder in which will be saved all images.
### Example
```bash
$ bash extractsImagesFromPdfFiles.sh "./pdfDocs" "./images"
```
