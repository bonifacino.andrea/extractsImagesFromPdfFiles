#/bin/sh
### Description: Extracts all images from pdf files from a root directory to its leaves
### Written by: Andrea Bonifacino - bonifacino.andrea@gmail.com on 10-2017

PDFROOT="./pdfDocs"
IMAGESFOLDER="./images"
PDFROOT=${1:-$PDFROOT}
IMAGESFOLDER=${2:-$IMAGESFOLDER}
COUT=0
STARTTIME=`date +%s.%N`
find $PDFROOT -name '*.pdf'| while read line; do
  FILENAME=$(basename $line)
  DIRNAME=${line#$PDFROOT}
  DIRNAME=${DIRNAME%$FILENAME}
  IMAGEDIR="$IMAGESFOLDER$DIRNAME"

  #echo "filename: $FILENAME"
  #echo "dirname: $DIRNAME"
  #echo "imagedir: $IMAGEDIR"

  mkdir -p $IMAGEDIR
  COUNT=$(( $COUNT + 1 ))
  echo "'$COUNT') Processing file '$line'"
  pdfimages -j -p $line $IMAGEDIR$FILENAME
done
ENDTIME=`date +%s.%N`
TIMEDIFF=`echo "$ENDTIME - $STARTTIME" | bc | awk -F"." '{print $1"."substr($2,1,3)}'`
echo "Full processing in $TIMEDIFF seconds"
